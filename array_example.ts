class Test {
  scores = [1,2,30,40,100];
  passedCandidates = [];
  
  // concat() example
  concatArrays() {
	  let arr1 : number[] = [1,2,3,4];
	  let arr2 : number[] = [5,6,7,8];
	  let arr3 = arr1.concat(arr2);
	  console.log(arr3);
  }
  
  // map() examples
  mapFn() {
	  let arr1:number[] = [1,2,3,4,5];
	  let arr2:number[] = arr1.map((item,index) =>{
		  return item * item;
	  });
	  console.log(arr2);
  }
  
  // reverse() example
  reverseFn() {
	  let arr1:number[] = [1,2,3,4,5];
	  let arr2:number[] = arr1.reverse();
	  console.log(arr2); // [ 5, 4, 3, 2, 1 ]
  }
  
  // sort() example
  sortFn() {
	  let names = ["Rajib", "Ananth", "Akshunn"];
	  let names1 = names.sort();
	  console.log(names1); // [ 'Akshunn', 'Ananth', 'Rajib' ]
	  
	  let arr1 : number[] = [100, 500, 200, 0, 700, 502, 1000];
	  let arr2 = arr1.sort();
	  console.log(arr2);// [ 0, 100, 1000, 200, 500, 502, 700 ]
	  
	  let arr3 = arr1.sort((a, b) => {
		  return a - b;
	  });
	  console.log(arr3);// [ 0, 100, 200, 500, 502, 700, 1000 ]
  }
  
  spreadOperator() {
	let arr1 = [1,2,3];
    let arr2 = [7,8,9];

    let arrNew1 = [...arr1]; // Same as arr1
    let arrNew2 = [...arr1, 4, 5, 6]; // 1,2,3,4,5,6
    let arrNew3 = [...arrNew2, ...arr2]; // 1,2,3,4,5,6,7,8,9
	console.log(arrNew3);
	
	let obj1 = {name:"rajib"};
    let obj2 = {...obj1, age:"100"}; // name, age
	console.log(obj2);
  }
  
  // forEach()
  forEachFn() {
	  let names = ["Rajib", "Ananth", "Akshunn"];
	  names.forEach((item, index) => {
		  console.log("Position : " + index + ", " + item);
	  });
  }
  
  // filter() examples
  filterPassedCandidates() {
	  let candidateScores:number[] = [1,2,30,40,100];
	  let passed:number[] = candidateScores.filter((item, index) => {
		  // Pass mark is 30
		  if(item >= 30) {
			  return item;
		  }
	  });
	  console.log("Passed Candidates marks:"+ passed);
  }

  filterArr() {
    let passed = this.scores.filter(this.filterCriteria);
    console.log("passed : " + passed)
    this.passedCandidates = passed;
  }

  filterCriteria(elem, index, arr) {
    if(elem >= 30) {
      return elem;
    }
  }
}


// Client code
let t:Test = new Test();
//t.mapFn();
//t.reverseFn();
//t.sortFn();
//t.spreadOperator();
t.forEachFn();
