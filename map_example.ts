let nameAgeMapping = new Map();
 
//Set entries
nameAgeMapping.set("Lokesh", 37);
nameAgeMapping.set("Raj", 35);
nameAgeMapping.set("John", 40);

//Iterate over map keys
for (let key of nameAgeMapping.keys()) {
    console.log(key);                   //Lokesh Raj John
}
 
//Iterate over map values
for (let value of nameAgeMapping.values()) {
    console.log(value);                 //37 35 40
}

console.log(nameAgeMapping.get("Lokesh"));    //37
console.log(nameAgeMapping.has("Lokesh"));       //true
nameAgeMapping.delete("Lokesh");    // true
nameAgeMapping.clear();             //Clear all entries


