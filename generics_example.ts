// Example of generics in typescript

function echo<T>(data : T) : T {
 return data;
}

let val = echo(2);
console.log(val + ":" + typeof val);    // 2:number

let val1 = echo("abc");
console.log(val1 + ":" + typeof val1);    // abc:string

let val2 = echo({name:"rajib"});
console.log(JSON.stringify(val2) + ":" + typeof val2);    // {"name":"rajib"}:object

let val3 = [1,2,3];
console.log(val3 + ":" + typeof val3);    // 1,2,3:object
